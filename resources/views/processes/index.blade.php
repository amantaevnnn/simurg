<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Processes</title>
</head>
<body>
<form action="{{ route('processes.index') }}" method="GET">
    <input type="text" name="search" value="{{ $search }}" placeholder="Search...">
    <button type="submit">Search</button>
</form>
<h1>Processes</h1>
<ul>
    @foreach ($processes as $process)
        <li><a href="{{ route('processes.show', $process) }}">{{ $process->name }}</a></li>
    @endforeach
</ul>
</body>
</html>

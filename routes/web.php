<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProcessController;

// Маршрут для отображения списка процессов
Route::get('/', [ProcessController::class, 'index'])->name('processes.index');
Route::get('/processes', [ProcessController::class, 'index'])->name('processes.index');

// Маршрут для отображения конкретного процесса
Route::get('/processes/{process}', [ProcessController::class, 'show'])->name('processes.show');

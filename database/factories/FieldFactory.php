<?php

namespace Database\Factories;

use App\Models\Field;
use Illuminate\Database\Eloquent\Factories\Factory;

class FieldFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Field::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->word,
            'type' => $this->faker->randomElement(['text', 'number', 'date']),
            'value' => $this->faker->word, // You can adjust this according to your needs
            'format' => $this->faker->randomElement(['%+.2f', 'Y-m-d', '%H:%M:%S']),
        ];
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Process;
use App\Models\Field;

class ProcessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Создаем 100 процессов
        Process::factory(100)->create()->each(function ($process) {
            // Для каждого процесса создаем от 1 до 5 полей
            Field::factory(rand(1, 5))->create(['process_id' => $process->id]);
        });
    }
}

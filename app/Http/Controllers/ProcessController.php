<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Process;

class ProcessController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');

        $processes = Process::when($search, function ($query, $search) {
            return $query->where('name', 'like', '%' . $search . '%');
        })->get();

        return view('processes.index', compact('processes', 'search'));
    }

    public function show(Process $process)
    {
        return view('processes.show', compact('process'));
    }
}
